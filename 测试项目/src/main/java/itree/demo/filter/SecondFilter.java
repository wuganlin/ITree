package itree.demo.filter;

import com.sise.itree.common.BaseFilter;
import com.sise.itree.common.annotation.Filter;
import com.sise.itree.model.ControllerRequest;

/**
 * @author idea
 * @data 2019/5/1
 */
@Filter(order = 2)
public class SecondFilter implements BaseFilter {

    @Override
    public void beforeFilter(ControllerRequest controllerRequest) {
        System.out.println("this is 2 before");
    }

    @Override
    public void afterFilter(ControllerRequest controllerRequest) {
        System.out.println("this is 2 after");
    }
}
